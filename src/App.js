import React from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Navbar from './components/navbar';

import Account from './pages/Account';
import Home from './pages/Home';
import Dashboard from './pages/Dashboard';
import Flight from './pages/Flight';

const App = () => (
  <Router>
    <div>
      <Navbar />
      <Route path='/home' component={Home} />
      <Route path='/dashboard' component={Dashboard} />
      <Route path='/account' component={Account} />
      <Route path='/flight/:number' component={Flight} />
    </div>
  </Router>
);

export default App;
