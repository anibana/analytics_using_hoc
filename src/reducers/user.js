import {FETCH_USER} from '../actions/action-types';
const defaultState = () => ({});

const userReducer = (state = defaultState(), action) => {
  switch (action.type) {
    case FETCH_USER:
      return {
        ...state,
        userId: 'random-identifier',
        name: 'Jon Snow',
        gender: 'Male'
      }
    default:
      return state;
  };
};

export default userReducer;
