export const provider = {
  id: 'PTFUI-123',
  applicationName: 'PTFUI',
  key: 'ABCD'
};

const selectUserDetails = ({user}) => ({
  name: user.name,
  gender: user.gender,
  userId: user.userId
});

export const pageTransition = {
  '/home': {
    value: 100,
    name: 'home',
    details: selectUserDetails
  },
  '/dashboard': {
    value: 200,
    name: 'dashboard',
    attr: 'some-random-data'
  },
  '/account': {
    value: 300,
    name: 'account',
    attr: 'another random data'
  },
  '/flight/:number': {
    value: 300,
    name: 'account',
    attr: 'another random data'
  },
  default: {
    value: 10
  }
};
