import React, {Component} from 'react';
import {withRouter} from 'react-router';
import {reduce, isFunction} from 'lodash';

const pageTransition = (config) => (props) => reduce(config, (acc, v, k) => {
  if (isFunction(v)) {
    return {
      ...acc,
      [k]: v(props)
    };
  }

  return {...acc, [k]: v};
}, {path: props.location.pathname});

export const withAnalytics = (config, provider) => (WrappedComponent) => {
  const getDisplayName = (component) => component.displayName || component.name || 'Component';

  class WithAnalytics extends Component {
    static displayName = `WithAnalytics(${getDisplayName(WrappedComponent)}`

    render() {
      provider.track(pageTransition(config)(this.props));

      return <WrappedComponent {...this.props} />;
    }
  }

  return withRouter(WithAnalytics);
}
