import {provider} from './analyticsConfig';
import {createAnalyticsProvider} from './analytics/createAnalyticsProvider';

export default createAnalyticsProvider(provider);
