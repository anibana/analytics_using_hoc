import React from 'react';
import {connect} from 'react-redux';
import {compose} from 'recompose';
import {withAnalytics} from '../analytics/withAnalytics';
import provider from '../analyticsProvider';

const Home = ({name, userId, gender}) => (
  <div>
    <div>
      <label>ID: </label>
      <span>{userId}</span>
    </div>
    <div>
      <label>Name: </label>
      <span>{name}</span>
    </div>
    <div>
      <label>Gender: </label>
      <span>{gender}</span>
    </div>
  </div>
);

const mapStateToProps = ({user}) => ({
  name: user.name,
  userId: user.userId,
  gender: user.gender
});

const mapPropsToEvent = {
  name: 'home',
  value: 100,
  details: ({name, userId, gender}) => ({
    name,
    gender,
    userId
  })
};

export default compose(connect(mapStateToProps), withAnalytics(mapPropsToEvent, provider))(Home);
