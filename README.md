# Route listener using HOC.

## Run in local

- yarn install
- yarn start

**Note:** *This is just an experiment on how the route listener will looks like. For now, Analytics provider will just console.log the events. Later we will replace it with an actual API call.*

**Another Note**: *analytics folder will be extracted to a node module. For now, for demonstration it is part of the React application*

### Sample Usage
```javascript
import React from 'react';
import {connect} from 'react-redux';
import {compose} from 'recompose';
import {withAnalytics} from '../analytics/withAnalytics';
import provider from '../analyticsProvider';

const Home = ({name, userId, gender}) => (
  <div>
    <div>
      <label>ID: </label>
      <span>{userId}</span>
    </div>
    <div>
      <label>Name: </label>
      <span>{name}</span>
    </div>
    <div>
      <label>Gender: </label>
      <span>{gender}</span>
    </div>
  </div>
);

const mapStateToProps = ({user}) => ({
  name: user.name,
  userId: user.userId,
  gender: user.gender
});

const mapPropsToEvent = {
  name: 'home',
  value: 100,
  details: ({name, userId, gender}) => ({
    name,
    gender,
    userId
  })
};

export default compose(connect(mapStateToProps), withAnalytics(mapPropsToEvent, provider))(Home);
```

### Sample output
```javascript
{path: "/home", value: 100, name: "home", applicationName: "PTFUI", id: "PTFUI-123", details: {name: "Jon Snow", gender: "Male", userId: "random-identifier"}}
```

### PROS
- More flexible, can pass dynamic data from store.
- Easy to test, since all data are read from props.
- Same approach as what redux is using.

### CONS
- Too much boilerplate - though i can be minimized later one.
- Config data is not centralized.

